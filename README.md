USAGE
=====
    
    python3 ltlf2rm.py <terf> <output_tlsf>
    

The code produces a reward machine in HOA format. The HOA is an automaton with transitions labeled with rewards.

DEPENDENCIES
============

Spot: https://spot.lrde.epita.fr/